# Repaso Clase III

Adaptado de [Bioinformatics at COMAV: Ejercicios de repaso](https://bioinf.comav.upv.es/courses/linux/python/ejercicios_repaso.html)

## Ejercicio 1

Escriba en mayúsculas la siguiente cadena de texto o *string*:

```
nombres = 'luis,federico,leloir'
```

## Ejercicio 2

Divida la anterior cadena de texto en una lista de palabras.

## Ejercicio 3

Guarde cada palabra de la cadena de texto anterior como una variable diferente.

## Ejercicio 4

Elimine los espacios y retornos de carro extra de la siguiente cadena de texto:

```
saludo = '   hola \nmundo'
```

## Ejercicio 5

Una la siguiente lista de palabras en una cadena de texto en la que las palabras estén separadas por comas:

```
palabras = ['una', 'lista', 'con', 'palabras']
```

## Ejercicio 6

Reemplace las comas por tabuladores en la siguiente cadena de texto:

```
numeros = 'una,dos,tres,cuatro'
```

## Ejercicio 7

¿Está la palabra “uno” contenida en las siguientes cadenas de texto?

```
string1 = 'tres, dos, uno'
string2 = 'spam, spam, spam'
```

## Ejercicio 8

Seleccione los primeros tres elementos de la siguiente línea. Después, los tres últimos.

```
lista2 = ['uno', 'dos', 'tres', 'cuatro', 5, 6, 7]
```

## Ejercicio 9

Calcule el número de elementos de los que se compone la lista anterior.

## Ejercicio 10

Dada la siguiente lista, calcule el número de elementos no repetitivos de la misma.

```
lista3 = ['uno', 'dos', 'tres', 'dos', 4, 5, 6, 6, 7, 'tres']
```

## Ejercicio 11

¿Cuál será el resultado que obtendremos al finalizar la ejecución de las cuatro líneas de código siguientes?

```
a = [1, 2, 3]
b = a 
b.append(4) 
print(a)
```

## Ejercicio 12

¿Qué pasaría si ejecutásemos el siguiente código? ¿Por qué se observa un comportamiento diferente al del ejercicio anterior?

```
a = '123'
b = a 
b = a + '4' 
print(b)
```

## Ejercicio 13

¿Comienzan las siguientes cadenas de texto por “#”? Si es así, elimine ese caracter.

```
string3 = '#explicacion de los campos'
string4 = 'campo1, campo2, campo2'
```

## Ejercicio 14

Dada la siguiente lista de cadenas de texto, filtre las que comienzan por “#” y obtenga una nueva lista.

```
lista1 = ["#explicacion", "campos", "#campo1", "campo2", "campo2"]
```

## Ejercicio 15

Imprima las claves o *keys* del siguiente diccionario. Luego, imprima sus valores. 

```
d = {1 : 'Noether',3: 'Isaac', 2: 'Albert' }
```

## Ejercicio 16

Realice lo mismo que en el ejercicio anterior, pero ahora imprima las claves en orden (recuerde que los diccionarios no necesariamente estarán ordenados). Después, imprima sus valores ordenados.

## Ejercicio 17

¿Está la key ‘hola’ en el siguiente diccionario?

```
d = {"spam": "hola", "monty": "spam"}
```

## Ejercicio 18

¿Cuál será el resultado de cada una de las siguientes líneas de código?

```
print(bool(1))
print(bool(0))
print(bool([1, 2]))
print(bool([]))
print(bool({}))
print(bool({1: 'si', 2: 'no'}))
print(bool('no'))
print(bool(''))
print(bool('1' and ''))
print(bool('' or '2'))
print(bool(not 'si'))
```

