Escribir una **función** que pida al usuario un número entre 1 y 10, calcule la tabla de multiplicar de ese número (del 1 al 10), escriba un archivo que se llame `tabla-n.txt` dónde `n` será el número ingresado por el usuario.
Cada línea del archivo deberá contener el resultado de la multiplicación.

El resultado debe ser algo similar a:

```
Introduce un número entero entre 1 y 10:
```
Si ingresamos `3` resulta en un archivo llamado `tabla-3.txt` cuyo contenido es:

```
3x1 3
3x2 6
3x3 9
3x4 12
3x5 15
3x6 18
3x7 21
3x8 24
3x9 27
3x10 30
```