* #### Clase VI. Python: Visualización de datos
    *   Generación de gráficos con Matplotlib, Seaborn, ggplot:
    *   Gráficos de dispersión
    *   Gráficos de barra
    *   Gráficos de líneas
    *   Gráficos de caja y bigote
    *   Histogramas
    *   Mapas de calor
    *   Personalización de gráficos.
    *   Gráficos para publicación.