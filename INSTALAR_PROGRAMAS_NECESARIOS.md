## :computer: Instrucciones para instalar paquetes y python en sus ordenadores.

* Si su ordenador tiene instalado Windows, cualquiera sea la versión, sólo necesita instalar la [Máquina Virtual](https://gitlab.com/nmoreyra/python-in-biology/blob/master/COMO_INSTALAR_LA_VM.md).
* Si su ordenador tiene Linux u OS (Mac) también puede utilizar la Máquina Virtual siguiendo las instrucciones del link anterior, pero le aconsejamos que instale los paquetes necesarios para poder trabajar directamente en su ordenador. Para ello debe realizar las siguientes operaciones:
	* Necesitamos tener instalado Python. Abra la terminal de comandos (`Ctrl + Alt + t` es un atajo comúnmente utilizado). Tipee `python3` y presione `Enter`, si en la pantalla aparecen el simbolo `>>>`, entonces lo tiene instalado. Puede salir de la interfaz de python tipeando `quit()` y, luego, `exit` para salir de la terminal de comandos.  
      * En caso de obtener un mensaje diciendo que **python3** no está instalado, tipee `sudo apt-get install python3` en linux. En OS debería ya estar instalado pero, en caso contrario, puede ver la manera de instalarlo [aquí](https://wsvincent.com/install-python3-mac/).  
	* Además, necesitamos tener instalado `git`. Es probable que su computadora ya lo tenga: abra la terminal de comandos como se indica antes y ejecute `git --version`, si en la pantalla aparece un mensaje similar a `git version 1.9.1`, entonces lo tiene instalado.
	  * En caso de ser necesario, en linux puede instalarlo con el comando `sudo apt-get install git`, y en OS (Mac) descargando y ejecutando el instalador gráfico [aquí](http://sourceforge.net/projects/git-osx-installer/). Para más detalles, consulte la [documentación oficial](https://git-scm.com/book/es/v1/Empezando-Instalando-Git).
	   
	  
### [Guía de instalación detallada para Mac, Linux y Windows (librerías, bash, jupyter y otros)](https://docs.google.com/document/d/142wPvA9D7oPMuty1OJl-ug7YjiqAMiXpFaOo_WHP7ns/edit?usp=sharing)
