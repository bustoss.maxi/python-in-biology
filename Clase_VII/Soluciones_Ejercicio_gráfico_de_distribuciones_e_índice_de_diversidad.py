import pandas as pd
import matplotlib.pyplot as plt
pd.set_option('display.expand_frame_repr', False)
#tutorial en: https://datacarpentry.org/python-ecology-lesson-es/05-merging-data/index.html
surveys_df = pd.read_csv(r"C:\Users\Nahuel\Desktop\surveys.csv", keep_default_na=False, na_values=[""])
species_df = pd.read_csv(r"C:\Users\Nahuel\Desktop\species.csv", keep_default_na=False, na_values=[""])
#Cree un nuevo DataFrame uniendo los contenidos de survey.csv y Tablas species.csv. Luego calcula y crea un gráfico de la distribución de:
#tomamos plot_id como parcela, y vemos cuantas taxas hay por parcela
#merged_inner va a ser nuestros dos dataframes unidos desde el "species_id"
merged_inner = pd.merge(left=surveys_df,right=species_df, left_on='species_id', right_on='species_id')
#print(surveys_df.shape)
#print(merged_inner.shape)
#print(merged_inner.head())
agrupado = merged_inner.groupby("plot_id")["taxa"].count()
# 1) taxa por parcela
"""grafico=agrupado.plot(kind= "bar");
grafico.set_ylabel("taxa por parcela")
grafico.set_xlabel("parcela")
plt.show()"""
# 2) taxa por sexo por parcela
agrupa2 = merged_inner.groupby( ["sex", "plot_id"] )["taxa"].count()
print(agrupa2)
grafico=agrupa2.plot(kind= "bar");
grafico.set_ylabel("cantidad de taxas")
grafico.set_xlabel("sexo por parcela")
plt.show()
