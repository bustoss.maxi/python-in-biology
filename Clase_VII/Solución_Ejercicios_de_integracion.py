import pandas as pd
import matplotlib.pyplot as plt
pd.set_option('display.expand_frame_repr', False)
surveys_df = pd.read_csv(r"C:\Users\Nahuel\Desktop\surveys.csv")
# 1) Selecciona un subconjunto de filas, en el DataFrame surveys_df, que contenga datos desde el año 1999 y que contenga valores en weight menores o iguales a 8.
#  ¿Cuántas filas obtuviste como resultado? ¿Cuántas filas obtuvo tu compañera?
df_desde_1999 = surveys_df[(surveys_df.year >= 1999) & (surveys_df.weight <= 8) ]
#print(df_desde_1999.size)
#se obtuvieron 495 filas
# 2) Usa la función isin para encontrar todos los plots que contienen una especie en particular en el DataFrame surveys_df. ¿Cuántos registros contienen esos valores?
# especie DM (Dipodomys	merriami)
especie_DM = surveys_df[surveys_df['species_id'].isin(["DM"])]
#print(especie_DM.count()) #hay 10596 especies DM
# 1)Experimenta con otras consultas. Crea una consulta que encuentre todas las filas con el valor de weight mayor o igual a 0.
valor_weight_mayor_a_0 = surveys_df[surveys_df.weight > 0]
#print(valor_weight_mayor_a_0)
# 2) El símbolo ~ puede ser usado en Python para obtener lo opuesto a los datos seleccionados que hayas especificado. Es equivalente a no esta en.
# Escribe una consulta que seleccione todas las filas con sex diferente de ‘M’ o ‘F’ en los datos de surveys_df
exclude_F_y_M = surveys_df[~surveys_df.sex.isin( ["M", "F"] )]
#print(exclude_F_y_M)
"""
Desafío - Revisando todo lo aprendido
1) Crea un nuevo objeto DataFrame que solamente contenga observaciones cuyos valores en la columna sex no sean female o male. 
Asigna cada valor de sex en el nuevo DataFrame a un nuevo valor de ‘x’. Determina el número total de valores null en el subconjunto.
"""
new_exclude_F_y_M = exclude_F_y_M.copy() #copia el df para no modificar el original
new_exclude_F_y_M["sex"] =new_exclude_F_y_M["sex"].fillna("x") #reemplaza los valores de "sex" por una "x"
#print(new_exclude_F_y_M)
#print(exclude_F_y_M[pd.isnull(exclude_F_y_M.sex)]) #hay 2511 valores NaN en la columna "sex"
"""
2) Crea un nuevo objeto DataFrame que contenga solo observaciones cuyos valores en la columna sex sean male o female y en los cuales el valor de weight sea mayor que 0.
Luego, crea un gráfico de barra apiladas del promedio de weight, por parcela, con valores male versus female apilados por cada parcela.
"""
df_M_y_F_weight_mayor_a_0 = surveys_df[(surveys_df["sex"].isin(["F", "M"])) & (surveys_df.weight > 0) ]
#print(df_M_y_F_weight_mayor_a_0)
prom_peso_parcela = df_M_y_F_weight_mayor_a_0.groupby(["plot_id", "sex"])["weight"].mean()
unstackeado= prom_peso_parcela.unstack()
print(unstackeado.plot(kind="bar").set_xlabel("parcela"));
plt.show()
