#### Reto de "graficación" final

Crea una gráfica de barras apiladas con el peso en el eje Y, y la variable de apilamiento que sea el sexo. La gráfica debe mostrar el peso total por sexo para cada sitio. Algúnos tips que te pueden ayudar a resolver el reto son los siguientes:

Para mayor información en gráfica con Pandas, visita la siguiente [link](https://pandas.pydata.org/pandas-docs/stable/user_guide/visualization.html).

Puedes usar el siguiente código para crear una gráfica de barras apiladas pero los datos para apilar deben de estar en distintas columnas. Aquí hay un pequeño ejemplo con algunos datos donde ‘a’, ‘b’ y ‘c’ son los grupos, y ‘one’ y ‘two’ son los subgrupos.

```
d = {'one' : pd.Series([1., 2., 3.], index=['a', 'b', 'c']),'two' : pd.Series([1., 2., 3., 4.], index=['a', 'b', 'c', 'd'])}
pd.DataFrame(d)
```


muestra los siguientes datos

```
      one  two
  a    1    1
  b    2    2
  c    3    3
  d  NaN    4
```

  
Podemos gráficar esto con

###### Graficar datos apilados de modo que las columnas 'one' y 'two' estén apiladas

```
my_df = pd.DataFrame(d)
my_df.plot(kind='bar',stacked=True,title="The title of my graph")
```
![Gráfico de prueba](https://datacarpentry.org/python-ecology-lesson-es/fig/stackedBar1.png)


*  Podemos usar el método `.unstack()` para transformar los datos agrupados en columnas para cada gráfica. Intenta ejecutar .unstack() en algún DataFrame anterior y observa a que lleva.

Empieza transformando los datos agrupados (por sitio y sexo) en una disposición desapilada entonces crea una gráfica apilada.