1) ¿Cuántos individuos son hembras **F** y cuántos son machos **M**?
2) Qué pasa cuando agrupas sobre dos columnas usando el siguiente enunciado y después tomas los valores medios:
```
grouped_data2 = surveys_df.groupby(['plot_id','sex'])
grouped_data2.mean()
```

3) Calcula las estadísticas descriptivas del peso para cada sitio. Sugerencia: puedes usar la siguiente sintaxis para solo crear las estadísticas para una columna de tus datos 
`by_site['weight'].describe()`


Una parte de la salida para en el 3) se ve así:

```
 site
 1     count    1903.000000
       mean       51.822911
       std        38.176670
       min         4.000000
       25%        30.000000
       50%        44.000000
       75%        53.000000
       max       231.000000
         ...
```
