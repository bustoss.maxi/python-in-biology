#### Desafío - Consultas


1.  Selecciona un subconjunto de filas, en el DataFrame *surveys_df*, que contenga datos desde el año 1999 y que contenga valores en **weight menores o iguales a 8**. ¿Cuántas filas obtuviste como resultado? ¿Cuántas filas obtuvo tu compañera?
2.  Puedes usar la función `isin` de Python para hacer una consulta a un DataFrame, basada en una lista de valores según se muestra a continuación:
  
```
surveys_df[surveys_df['species_id'].isin([listGoesHere])]
```

Usa la función `isin` para encontrar todos los **plots** que contienen una especie en particular en el DataFrame **surveys_df**. ¿Cuántos registros contienen esos valores?

1.  Experimenta con otras consultas. Crea una consulta que encuentre todas las filas con el valor de **weight** mayor o igual a 0.
2.  El símbolo `~` puede ser usado en Python para obtener lo **opuesto** a los datos seleccionados que hayas especificado. Es equivalente a **no esta en**. Escribe una consulta que seleccione todas las filas con **sex** diferente de ‘M’ o ‘F’ en los datos de **surveys_df**.


#### Desafío - Revisando todo lo aprendido

1.  Crea un nuevo objeto DataFrame que solamente contenga observaciones cuyos valores en la columna sex no sean female o male. Asigna cada valor de sex en el nuevo DataFrame a un nuevo valor de ‘x’. Determina el número total de valores null en el subconjunto.
2.  Crea un nuevo objeto DataFrame que contenga solo observaciones cuyos valores en la columna sex sean male o female y en los cuales el valor de **weight sea mayor que 0**. Luego, crea un gráfico de barra apiladas del promedio de weight, por parcela, con valores male versus female apilados por cada parcela.