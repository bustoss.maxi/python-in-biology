* #### Clase I. Configuración inicial
	* [Introducción a la Bioinformática. Desarrollo de software para ciencias biológicas.](https://docs.google.com/presentation/d/1sg0wpm9P-53uyDzV0UZRHZUmPI7DOX6ZLlJyViEV5-k/edit?usp=sharing)
	* [Unix/Linux: organización, arquitectura, sistema de archivos.](Clase_I/IntroduccionLinux.pdf) :penguin:
	* La terminal de Unix. ![](Clase_I/imgs/terminal2.png)
	* [Introducción a la terminal.](https://swcarpentry.github.io/shell-novice-es/01-intro/index.html)
		*  [Planilla de resumen de comandos básicos](Clase_I/Linux-comandos-basicos.pdf)
		* [Navegación de archivos y directorios.](https://swcarpentry.github.io/shell-novice-es/02-filedir/index.html)
		* [Trabajando con archivos y directorios.](https://swcarpentry.github.io/shell-novice-es/03-create/index.html)
		* [*Pipes* y filtros](https://swcarpentry.github.io/shell-novice-es/04-pipefilter/index.html)
		* [Bucles](https://swcarpentry.github.io/shell-novice-es/05-loop/index.html)
		* [Scripts de la terminal](https://swcarpentry.github.io/shell-novice-es/06-script/index.html)
	* Python: fundamentos, versiones y distribuciones, instalación y ejecución, instalación de librerías, ayuda disponible.
		* [Diapositivas "Introducción a Python"](Clase_I/Introducción_a_Python.pdf)
		* [Invocando al intérprete](http://docs.python.org.ar/tutorial/3/interpreter.html#invocando-al-interprete)
		* [Breve introducción a la Programación en Python](https://datacarpentry.org/python-ecology-lesson-es/01-short-introduction-to-Python/index.html)
	* Primer programa.
	* Guías, tutoriales y cursos de la Clase I.  
        *  [Las mejores guias de python.](http://www.python.org.ar/wiki/AprendiendoPython)
		*  [Codecademy.](https://www.codecademy.com/catalog/subject/all)
		*  [Lista de libros de programación I](https://github.com/EbookFoundation/free-programming-books/blob/master/free-programming-books.md)
		*  [Lista de libros de programación II](https://github.com/EbookFoundation/free-programming-books/blob/master/free-programming-books-es.md)
		*  [Curso de python online.](https://codigofacilito.com/courses/Python)
		*  [Otro curso de python.](https://teachlr.com/cursos-online/curso-basico-de-python/)
		*  [Instalación de software y configuración de entorno de trabajo local y online.](https://www.digitalocean.com/community/tutorials/how-to-install-python-3-and-set-up-a-local-programming-environment-on-ubuntu-16-04)
		*  [Página de cursos de todo un poco.](https://www.tareasplus.com/)