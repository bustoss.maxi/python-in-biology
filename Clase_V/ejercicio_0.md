#### Ejercicio de repaso

Crear una lista que consista en las dos listas de números ([primernumbers.txt](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_V/primenumbers.txt) y [happynumbers.txt](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_V/happynumbers.txt)) solapadas.
Para ello deberá **abrir los archivos de texto**, explorar linea por línea (con un ciclo **for** o un **while**), utilizar **métodos de lista**. También puede hacerlo escribiendo una **función**.

Para ver la solución clickee [aquí](https://www.practicepython.org/solution/2014/12/25/23-file-overlap-solutions.html)