* #### Clase V. Python: Manejo de datos

* #### Requisitos
    *   Instalar numpy, pandas y matplotlib usando *pip*. Para eso, abrir una terminal de Linux o MacOS e instalar con el comando *pip install numpy pandas matplotlib*. La instalación en Windows es ligeramente distinta; pueden seguir [este tutorial](https://datatofish.com/install-package-python-using-pip/).
    *   Si lo anterior no funcionó, descargar [pandas](https://files.pythonhosted.org/packages/07/cf/1b6917426a9a16fd79d56385d0d907f344188558337d6b81196792f857e9/pandas-0.25.1.tar.gz) y [matplotlib](https://files.pythonhosted.org/packages/12/d1/7b12cd79c791348cb0c78ce6e7d16bd72992f13c9f1e8e43d2725a6d8adf/matplotlib-3.1.1.tar.gz). Luego, abrir una terminal de Linux o MacOS e instalar con el comando *pip install <path-al-archivo>*, comenzando por pandas y luego matplotlib.
    *   Además, deben descargar los archivos [surveys.csv](https://ndownloader.figshare.com/files/10717177) y [species.csv](https://ndownloader.figshare.com/files/3299483).  
  
* #### Contenidos
    *   [Tablas de datos con Pandas](https://datacarpentry.org/python-ecology-lesson-es/08-putting-it-all-together/index.html)
    *   [Lectura y escritura de datos estructurados.](https://datacarpentry.org/python-ecology-lesson-es/02-starting-with-data/index.html)
    *   [Manipulación de dataframes](https://datacarpentry.org/python-ecology-lesson-es/03-index-slice-subset/index.html)
    *   [Tipos de datos y formatos / escribir archivo .csv](https://datacarpentry.org/python-ecology-lesson-es/04-data-types-and-format/index.html)
    *   [Combinando dataframes](https://datacarpentry.org/python-ecology-lesson-es/05-merging-data/index.html)
    *   Datos en grupos.
    *   Datos faltantes.
    *   Estadística descriptiva.
    *   [Tabla 'surveys.csv'](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_V/surveys.csv)
    
