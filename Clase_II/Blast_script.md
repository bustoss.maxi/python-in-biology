## Automatización de tareas.

Como ya vimos la clase pasada, una manera de automatizar tareas es mediante un script, que por lo general se almacena en un archivo de texto plano. 
En GNU/Linux los scripts suelen ser identificados por el sistema a través un encabezado contenido del archivo conocido como **shebang**: `#!/bin/bash`, que detallaremos más adelante.
**gedit** es un editor de texto plano que utilizaremos para crear un script.

Crear una nueva carpeta `mis_scripts` en la carpeta `Clase_II` y acceder a ella. Luego, ejecutar `gedit`:

```bash
cd /home/alumno/Clase_II
mkdir mis_scripts && cd mis_scripts
gedit & #el símbolo & hace que gedit se corra en segundo plano y permita seguir interactuando en la terminal.
```

Copiar y pegar las siguientes líneas:

```bash
#!/bin/bash

echo 'Hola! Este programa hace esto'
```

Guardar el archivo como **programa.sh**, salir de **gedit** y verificar los permisos del mismo.¿Es ejecutable?

```bash
ls -l programa.sh
```

Asignar permisos de ejecución y verificar nuevamente los cambios:

```bash
chmod +x programa.sh
ls -l programa.sh
```

Correr el programa. Para esto, usar `./` o `bash` adelante del archivo o la ruta completa:

```bash
./programa.sh
#ó
bash programa.sh
```

Añadir una línea de código al final del programa y volver a ejecutar:

```bash
echo 'date' >> programa.sh
./programa.sh
```

Añadir mediante *nano* la siguiente línea de código al final del programa: `echo $1`:

```bash
nano programa.sh
```

*Hint: CTRL + o para guardar, CTRL + x para salir*

Ejecutar programa añadiendo una oración entre comillas a continuación:

```bash
./programa.sh "esto fue todo, amigues..."
```

`$1` es una variable (argumento) que se encuentra en primera posición cuando corremos un script en el shell. 
Podríamos hacer un script para correr blast como antes pero usando variables, de tal forma que ingresemos los datos particulares según se necesiten.


Crear un archivo ejecutable llamado blastx.script que diga lo siguiente:

```bash
#!/bin/bash

blastx -db $1 -query $2 -out $3 -outfmt 6
```

¿Cómo lo corremos? Vamos paso a paso.

La primera línea es denominada **shebang**, y lo que hace es decirle al interprete, que es shell, cual es el programa que va a utilizar para interpretar y ejecutar este script.
En este caso, lo hará mediante bash, donde cada linea de este código equivale a un comando en la terminal.

Primero, me ubico donde generaré el script:

```bash
cd /home/alumno/Clase_II/mis_scripts
```

Checkeo el contenido de la carpeta donde se encuentran los archivos que necesito:

```bash
ls /home/alumno/Clase_II/secuencias_blast
# ó
ls ../secuencias_blast
```

A modo de recordatorio, en el siguiente comando `-db` es el parámetro que se ingresa para indicarle al programa blastx que se ingresará el nombre del archivo de la base de datos (con su ruta, si es necesario).
`-query` es el parámetro que le indice al programa cual es la secuencia de consulta y `-out` es el parámetro que le dice al programa cual será el nombre del archivo (con su ruta, si es necesario).`-outfmt 6` es un parámetro de blast que devolverá una tabla tabulada y más fácil de leer (los nombres de las columnas no están incluídos pero pueden buscarlas en el manual de blast).

Genero el script con el nombre **blastx_script.sh**. Para ellos, elija su editor preferido entre los que conoce, guárdelo y asígnele los permisos necesarios para su ejecución:

```bash
#!/bin/bash

blastx -db $1 -query $2 -out $3 -outfmt 6
```

En este ejemplo, `$1`, `$2` y `$3` son variables del lenguaje bash que permiten interactuar con el usuario mediante la terminal e indican los argumentos ingresados según su orden de aparición. 
Por esto, este script se corre de la siguiente manera (en la terminal):

```bash
./blastx_script.sh ../secuencias_blast/db_prot_human ../secuencias_blast/query.fas ../trabajo/salida_blastx_script.table
```

En este caso, la primer variable ($1) tomará el nombre de la base de datos, la segunda ($2) tomará el nombre de la secuencia de consulta y $3 tomará el nombre de la salida.

Inspeccione la salida.¿Cuántas líneas tiene?

```bash
less -S ../trabajo/salida_blastx_script.table
wc -l ../trabajo/salida_blastx_script.table
```

