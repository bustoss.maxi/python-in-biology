 * #### Clase III. Python: Variables y control de flujo
	 * Sintaxis general
	 	* [Carpentry](https://hpc-carpentry.github.io/hpc-python/01-basics/)
	 	* [Carpentry lecciones de ecología](https://datacarpentry.org/python-ecology-lesson/01-short-introduction-to-Python/index.html)
	 * [Guías de estilo: PEP8](https://www.python.org/dev/peps/pep-0008/#maximum-line-length) 
	 * Definición y uso de variables
	    * [Variables 1](http://www.mclibre.org/consultar/python/lecciones/python-variables.html)
	    * [Variables 2](http://www.mclibre.org/consultar/python/lecciones/python-variables-2.html)
	    * [Variables 3](http://www.mclibre.org/consultar/python/lecciones/python-variables-3.html)
	 * Operadores:
	    * [matemáticos +  -  *  /  %  //](http://www.mclibre.org/consultar/python/lecciones/python-operaciones-matematicas.html)
	    * [relacionales ==  >  < !=  <=  >=](https://entrenamiento-python-basico.readthedocs.io/es/latest/leccion3/operadores_relacionales.html)
	 * Tipos de objetos básicos: 
	    * [int (enteros) y float (decimales)](http://www.mclibre.org/consultar/python/lecciones/python-operaciones-matematicas.html#enteros-decimales)
	    * [string (cadenas)](http://www.mclibre.org/consultar/python/lecciones/python-cadenas.html)
	    * lista
	    	* [mclibre.org](http://www.mclibre.org/consultar/python/lecciones/python-listas.html) y [Bioinformatics at COMAV en español](https://bioinf.comav.upv.es/courses/linux/python/listas.html)
	    * [diccionarios y sets](https://bioinf.comav.upv.es/courses/linux/python/estructuras_de_datos.html)
	    * [tupla](http://www.mclibre.org/consultar/python/lecciones/python-tupla.html)
	 * Estructuras de control: if, elif, for, while.
	    * [Control de flujo](https://bioinf.comav.upv.es/courses/linux/python/control_de_flujo.html)
	    * [Loops](http://swcarpentry.github.io/python-novice-inflammation/02-loop/index.html) - [loop for en carpentry](http://swcarpentry.github.io/python-novice-gapminder/12-for-loops/index.html)
	    * [if,elif,else](http://swcarpentry.github.io/python-novice-gapminder/17-conditionals/index.html)
	    * [Loop while](http://www.mclibre.org/consultar/python/lecciones/python-while.html)
	 * Ejercicios resueltos en clase:
	    * [Ejercicio_0 (input)](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_III/ejercicio_0.md)
	    * [Ejercicio_1 (editar string)](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_III/ejercicio_1.md)
	    * [Ejercicio_2 (intro listas)](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_III/ejercicio_2.md)
	    * [Ejercicio_3 (condicionales y bucles)](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_III/ejercicio_3.md)
	    * [Ejercicio_4 (diccionarios)](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_III/ejercicio_4.md)
	 * Guías, tutoriales y cursos de la Clase III
	    * [Guia completa sintaxis Python (pdf)](https://www.iaa.csic.es/python/curso-python-para-principiantes.pdf)
	    * [Manual Python (pdf)](http://docs.python.org.ar/tutorial/pdfs/TutorialPython2.pdf)